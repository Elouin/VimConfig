default: link

submodules:
	git submodule init
	git submodule update

link: submodules
	ln -vsf ${PWD}/vimrc ${HOME}/.vimrc
	ln -vsf ${PWD}/vim ${HOME}/.vim

unlink:
	unlink ${HOME}/.vimrc
	unlink ${HOME}/.vim
