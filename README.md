# Vim Config

My vim config. I am aiming for few plugins, since vim is capable
of doing most of what plugins do by itself. Also trying to keep the
vimrc as small as possible.

## Install

1. Clone the repository somewhere
2. run `make link`

## Used Plugins

### Airline

A custom status line. Main feature i missed from the default one
is the buffer line at the top.

### Catppuccin Theme

I currently use the `catppuccin_mocha` theme, so thats why i need this plugin.

### Hy language

Plugin for the hy programming language.

### Crystal language

Plugin for the crystal programming language.
