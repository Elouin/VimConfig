" Vim conf of the cool h4x0r guy

" Only the truth may be told
if exists('+termguicolors')
    let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
endif

" Does things i dont need to understand
set nocompatible
set hidden
set nostartofline

" Gives the most bootiful linenumbers
set number
set relativenumber

" Intelligent indentationes
filetype indent plugin on
set backspace=indent,eol,start
set autoindent

" The syntax is my highlight
syntax on

" Those lines do break
set linebreak

" The path you choose
set path+=**

" The most beatufil command-line completion
set wildmenu

" Searching without cases
set ignorecase
set smartcase

" Dont lose dat line Bro
set laststatus=2

" Dont lose dat work Bro
set confirm

" Less beep more bling
set visualbell
set t_vb=

" Klicking in ma Terminal
set mouse=a

" Timeout
"set notimeout ttimeout ttimeoutlen=200

" Those religous things shouldnt be missing
set shiftwidth=4
set softtabstop=4
set expandtab

" Searching marks will disappear
nnoremap <C-L> :nohl<CR><C-L>

" I like it colorful
colorscheme catppuccin_mocha
let g:airline_theme='catppuccin_mocha'

" Complete is what i desire
filetype plugin on
set omnifunc=syntaxcomplete#Complete

" A man is not a man without a Tree
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
" let g:netrw_altv = 1
" let g:netrw_winsize = 15
" augroup ProjectDrawer
"   autocmd!
"   autocmd VimEnter * :Vexplore
" augroup END

" yaml indentation
au FileType yaml setlocal tabstop=2 expandtab shiftwidth=2 softtabstop=2
au FileType crystal setlocal tabstop=2 expandtab shiftwidth=2 softtabstop=2

" clip to ma board boy
set clipboard=unnamedplus

" airline in da air
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

" terminaliate
"set termwinsize=12x0

" splitting is hitting
set splitbelow
